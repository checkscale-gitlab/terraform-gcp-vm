# terraform-gcp-vm

Provision Linux VM in GCP Google Cloud

### Prerequisites

* Have a google cloud account

## STEPS
1. Create a google project in the google console.

1. Create Service account through api credential with name `terraform_deployer` in [serviceaccountkey](https://console.cloud.google.com/apis/credentials/).

1. Create a key for the new service account credential in `json` format. Download and keep them safe.

1. Set IAM Credential with *Editor*, *Admin* permissions over `Computer Engine`.

1. Clone repository `git@gitlab.com:enperez/terraform-gcp-wordpress.git`
```bash
git clone git@gitlab.com:enperez/terraform-gcp-wordpress.git
cd terraform-gcp-wordpress
```

### Authentication
When json credential is created on the service account, a json file holding creds is downloaded only once.

Authentication can be done by setting path for account file in `terraform` or by setting google `environment` var. By default this proyect uses terraform method.

- `environment`: Set system environment path variable for credential file.
```
export GOOGLE_CLOUD_KEYFILE_JSON={{path_account_file.json}}
```
- `terraform`: Adding the credential argument into `provider.tf`
```
credentials = file("config/account_file.json")
```
> The credential file can be stored in project repository if `.gitignore` is used to avoid pushing credentials.

### Deploy

1. Adjust desired vars in `config/vars.tfvars` file:
> `project, region, zone, vm_type, name, imagedisk`

1. Init the terraform backend
```
terraform init
```

1. Generate and show the plan
```
terraform plan -var-file=config/vars.tfvars
```

1. Apply the plan
```
terraform apply -var-file=config/vars.tfvars
```

### How to Uninstall
* Destroy the resources with a single terraform command
```
terraform destroy -var-file=config/vars.tfvars
```
